#!/usr/bin/env python
import sys
import os
location = ''.join([os.getcwd(), '/'])
sys.path.append(location)
import woot
woots = woot.processData(woot.gatherLinks())
alldata = woot.writeData(woots, loc=location, filename='woots.data', smallfile='woots.json')
woot.writeRSS(alldata, loc=location, filename='woots.rss')
woot.writeXML(woots, loc=location, filename='woots.xml')

woots = woot.processData(woot.gatherLinks())
alldata = woot.writeData(woots, loc=location, filename='woots.data', smallfile='woots.json')
woot.writeRSS(alldata, loc=location, filename='woots.rss')
woot.writeXML(woots, loc=location, filename='woots.xml')

# Check  filesize to make sure they aren't empty
jsonsize = os.path.getsize(''.join([location, 'woots.json']))
xmlsize = os.path.getsize(''.join([location, 'woots.xml']))

if jsonsize < 5:
  raise Exception("JSON is empty...")

if xmlsize < 125:
  raise Exception("XML is empty...")
