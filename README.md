# Woot Deals

This is a basic library to pull the latest Woot Daily Deal from their
website and present them in an easy to aggregate format. Storage is handled
using a [JSON][] file.  No database required.

Working product: `Coming Soon`

[ ![Codeship Status for bttl/pill-woot](https://www.codeship.io/projects/c14a44c0-37a8-0131-d74f-0ac9200b116b/status?branch=master)](https://www.codeship.io/projects/9960)

## Usage:

```python
>>> import woot
>>> woots = woot.processData(woot.gatherLinks())
>>> alldata = woot.writeData(woots, loc='./', filename='data.json', smallfile='woots.json')
>>> woot.writeRSS(alldata, loc='./', filename='data.rss')
>>> woot.writeXML(woots, loc='./', filename='data.xml')
```

### `gatherLinks()`

Get all the deal sites from Woot. If they add more sites (ie Tools, Kids),
this should automatically add them to the list of sites to process.

### `processData(wootlinks)`

Walks through all the sites from `gatherLinks()` and process all the new deals
for use later.

### `writeData(data, loc='./', filename='data.json', smallfile='woots.json')`

Writes the deals to a json file `woots.json` and appends the data to the
seven day running storage `data.json`.

This will also return a Dict with the past 7 days worth of stored data for use
in the `writeRSS` function.

### `writeRSS(data, loc='./', filename='data.rss')`

Writes your output [RSS 2.0][] file for use in your tool|aggregator.

Only writes the past 3 days worth of deals.

### `writeXML(data, loc='./', filename='data.xml')`

Writes your output [XML][] file for use in your tool.

## TODO

- Add more output types
    - [Atom][ATOM]
    - CSV _(Maybe...)_
- Use Firebase (Maybe...)
- Create example run script


[RSS 2.0]: http://cyber.law.harvard.edu/rss/rss.html
[JSON]: http://www.json.org/
[XML]: http://www.w3.org/TR/REC-xml/
[ATOM]: http://www.atomenabled.org/developers/syndication/atom-format-spec.php
