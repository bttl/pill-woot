"""Woot API
http://bitbucket.org/bttl/wootpill

Woot API provides a way to get the deals from Woot's network of daily deal
sites.  This doesn't work on WootOffs.

Dependencies include:
  PyQuery>=1.2.4

Current outputs include:
  JSON        -> writeData()
  RSS 2.0     -> writeRSS()
  XML 1.0     -> writeXML()

License:

Copyright (c) 2013, Trae Blain
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of Trae Blain nor BoTTLe Tools nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
import re
import os
import json
from pyquery import PyQuery as pq
from datetime import datetime as dt
from datetime import timedelta
import hashlib
from lxml import etree

__version__ = "0.1"
__author__ = "Trae Blain <http://traebla.in/>"
__copyright__ = "Copyright (c) 2013 Trae Blain"
__license__ = "Modified BSD"


def gatherLinks():
  """Grabs the links to all the Woot Products. Home/Tech/Kids/etc.

  New ones are being added so this automatically adds them to this API.
  """
  data = pq(url="http://www.woot.com/", parser="html")
  links = [pq(i)('a').attr.href for i in data('.wootplus')]
  link_regex = re.compile("^(http://(.+?)\..+?)[#]")
  woots = {}
  mapping = {'www': 'woot'}
  for i in links:
    a = link_regex.match(i).groups()
    if a[1] in mapping:
      woots[mapping[a[1]]] = {'url': a[0]}
    else:
      woots[a[1]] = {'url': a[0]}
  return woots


def processData(woots):
  """Process all the data and provide a concise Dict with all the information.
  """
  newWoots = {}
  for key, value in woots.iteritems():
    newWoots[key] = value
    data = pq(url=value["url"], parser="html")
    data.make_links_absolute()
    newWoots[key]['title'] = data('h2.fn').text()
    price_regex = re.compile('(\$[0-9,]+?\.[0-9]{2})')
    newWoots[key]['price'] = price_regex.match(
                              data('#summary .price').text()).group()
    newWoots[key]['buy'] = data('a.wantone').attr.href
    newWoots[key]['bigImage'] = data('#todays-deal .photo').attr.src
    newWoots[key]['smallImage'] = pq(data('.wootplus a.tab.current')
                                     .parents('li')
                                     )('a.photo img').attr.src
    newWoots[key]['copy'] = data('.excerpt').text()[:-17]
    newWoots[key]['isodate'] = dt.now().isoformat()
    newWoots[key]['date'] = dt.now().strftime('%A, %B %d %Y %H:%M:%S CST')
    m = hashlib.md5()
    m.update(newWoots[key]['buy'])
    newWoots[key]['hash'] = m.hexdigest()

  return newWoots


def writeData(data, loc='./', filename='data.json', smallfile='woots.json'):
  """Write the latest Woot deals to a JSON file.

  Also returns a dict of all the stored Woot data of the past 7 days.
  """
  if not os.path.exists(os.path.join(os.path.abspath(loc), filename)):
    newFile = open(os.path.join(os.path.abspath(loc), filename), 'w')
    newFile.write('{}')
    newFile.close()

  existing = open(os.path.join(os.path.abspath(loc), filename), 'r')
  allData = json.loads(existing.read())
  existing.close()

  now = dt.now().strftime('%d%m%Y')
  allData[now] = data

  toDelete = []
  for item in allData:
    itemdate = dt.strptime(item, "%d%m%Y")
    if itemdate < dt.now() - timedelta(days=7):
      toDelete.append(item)
  for item in toDelete:
    del allData[item]

  wfile = open(os.path.join(os.path.abspath(loc), filename), 'w')
  wfile.write(json.dumps(allData, sort_keys=True, indent=2))
  wfile.close()

  sfile = open(os.path.join(os.path.abspath(loc), smallfile), 'w')
  sfile.write(json.dumps(data, sort_keys=True, indent=2))
  sfile.close()
  return allData


def writeXML(data, loc='./', filename='data.xml'):
  """Wrie the latest Woot deals to an XML file.
  """
  tree = etree.Element('woots')

  for key, value in data.iteritems():
    subItem = etree.SubElement(tree, 'item')
    etree.SubElement(subItem, 'type').text = key
    etree.SubElement(subItem, 'title').text = value['title']
    etree.SubElement(subItem, 'price').text = value['price']
    etree.SubElement(subItem, 'link').text = value['buy']
    etree.SubElement(subItem, 'siteurl').text = value['url']
    etree.SubElement(subItem, 'copy').text = etree.CDATA(
                                                  value['copy'])
    etree.SubElement(subItem, 'pubDate').text = value['date']
    etree.SubElement(subItem, 'bigImage').text = value['bigImage']
    etree.SubElement(subItem, 'smallImage').text = value['smallImage']
  tree.append(etree.Comment("Brought to you by http://traebla.in/ http://bt.tl\
"))

  wfile = open(os.path.join(os.path.abspath(loc), filename), 'w')
  wfile.write(etree.tostring(tree,
                             pretty_print=True,
                             xml_declaration=True,
                             encoding='utf-8'))
  wfile.close()
  return


def writeRSS(data, loc='./', filename='data.rss'):
  """Write RSS 2.0 data.  This writes the latest ~3 dats of entries, sorted by
  date, truncating any additional data.

  Currently old entries aren't stored, so this needs to read the RSS file also.
  """
  d = sorted(data, key=lambda x: dt.strptime(x, '%d%m%Y'), reverse=True)
  now = dt.now().strftime("%A, %B %d %Y %H:%M:%S CST")

  tree = etree.Element('rss', version="2.0")
  channel = etree.SubElement(tree, 'channel')
  etree.SubElement(channel, 'title').text = "Woots"
  etree.SubElement(channel, 'link').text = "http://bt.tl/pills/#woot"
  etree.SubElement(channel, 'description').text = "Feed of Woot's Daily \
Deals brought to you by Trae Blain.  http://traebla.in/ http://bt.tl/"
  etree.SubElement(channel, 'language').text = "en-us"
  etree.SubElement(channel, 'pubDate').text = now
  etree.SubElement(channel, 'lastBuildDate').text = now
  etree.SubElement(channel, 'docs').text = "http://bt.tl/pills/#woot"
  etree.SubElement(channel, 'managingEditor').text = "trae@traeblain.com"
  etree.SubElement(channel, 'webMaster').text = "trae@traeblain.com"

  for i in d[:3]:
    for key, value in data[i].iteritems():
      subItem = etree.SubElement(channel, 'item')
      etree.SubElement(subItem, 'title').text = value['title']
      etree.SubElement(subItem, 'link').text = value['buy']
      etree.SubElement(subItem, 'category').text = key
      etree.SubElement(subItem,
                       'description').text = etree.CDATA(''.join([
                                            '<img src="',
                                            value['smallImage'],
                                            '" style="float:left"><h2>',
                                            value['title'],
                                            '</h2><h3>',
                                            value['price'],
                                            '</h3><p>',
                                            value['copy'],
                                            '</p><p style="font-size:2em"><a \
href="',
                                            value['buy'],
                                            '">I Want One!</a></p><p style="\
font-size:0.6em">Brought to you by <a href="https://bt.tl/">Bottled Tools</a>\
</p>']))
      etree.SubElement(subItem, 'pubDate').text = value['date']
      etree.SubElement(subItem, 'guid').text = value['hash']

  wfile = open(os.path.join(os.path.abspath(loc), filename), 'w')
  wfile.write(etree.tostring(tree,
                             pretty_print=True,
                             xml_declaration=True,
                             encoding='utf-8'))
  wfile.close()
  return
